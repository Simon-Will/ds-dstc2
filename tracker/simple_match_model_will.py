#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import itertools
import re
import sys
from typing import List, Sequence

import numpy as np

from tracker.dataset.dstc2 import Dstc2
from tracker.evaluation import evaluate, format_results

CompiledRegex = type(re.compile('dummy regex'))
RegexMatch = type(re.match('a', 'a'))


def find_first_idx(haystack, needle):
    for i, elm in enumerate(haystack):
        if elm == needle:
            return i
    else:
        return None


class SimpleMatchModel:

    def __init__(self, words_vocab, labels_vocab, labels_vocab_separate,
                 delim=Dstc2.sys_usr_delim, food_idx=0, area_idx=1,
                 pricerange_idx=2):
        self.words_vocab = words_vocab
        self.labels_vocab = labels_vocab
        self.food_vocab, self.area_vocab, self.pricerange_vocab = (
            labels_vocab_separate)
        self.delim = delim
        self.delim_vocab_idx = self.words_vocab.get_i(delim)

        self.food_idx = food_idx
        self.area_idx = area_idx
        self.pricerange_idx = pricerange_idx

        self.default_labels = (
            self.food_vocab.get_i('none'),
            self.area_vocab.get_i('none'),
            self.pricerange_vocab.get_i('none'),
        )

        self.food_regex = self.make_regex(self.food_vocab)
        self.area_regex = self.make_regex(self.area_vocab)
        self.pricerange_regex = self.make_regex(self.pricerange_vocab)

    def make_regex(self, vocab):
        return re.compile('|'.join(re.escape(phrase)
                                   for phrase in vocab._w2int
                                   if phrase not in vocab.extra_words
                                   and phrase != 'none'))

    def get_matches(self, utterance: str,
                    phrase_regex: CompiledRegex) -> List[RegexMatch]:
        return list(phrase_regex.finditer(utterance))

    def utterance_array_to_str(self, utterance: Sequence[int]) -> str:
        return ' '.join(self.words_vocab.get_w(idx) for idx in utterance)

    def predict(self, inputs, labels_shape):
        predicted_labels = np.zeros(labels_shape, dtype=int)
        for dialog_idx, turn_idx, (turns, turn_lens, prev_labels) in inputs:
            utterances_as_indices = itertools.chain.from_iterable(
                turn[find_first_idx(turn, self.delim_vocab_idx) + 1:turn_len]
                for turn, turn_len in zip(turns, turn_lens)
            )
            all_usr_utterances = self.utterance_array_to_str(
                utterances_as_indices)

            usr_food_matches = self.get_matches(all_usr_utterances,
                                                self.food_regex)
            usr_area_matches = self.get_matches(all_usr_utterances,
                                                self.area_regex)
            usr_pricerange_matches = self.get_matches(all_usr_utterances,
                                                      self.pricerange_regex)

            new_labels = list(self.default_labels)
            if usr_food_matches:
                new_labels[self.food_idx] = self.food_vocab.get_i(
                    usr_food_matches[-1].group()
                )
            if usr_area_matches:
                new_labels[self.area_idx] = self.area_vocab.get_i(
                    usr_area_matches[-1].group()
                )
            if usr_pricerange_matches:
                new_labels[self.pricerange_idx] = self.pricerange_vocab.get_i(
                    usr_pricerange_matches[-1].group()
                )

            predicted_labels[dialog_idx][turn_idx] = new_labels

        return predicted_labels


def get_args():
    ap = argparse.ArgumentParser(__doc__)
    ap.add_argument('--dataset_first_n', default=None,
                    help='Reduce the datasets to first_n examples')
    args = ap.parse_args()
    return args


def print_stats(train, valid, test):
    print('Stats ===============')
    print('Train classes: %d' % np.max(train.labels))
    print('Valid classes: %d' % np.max(valid.labels))
    print('Test classes: %d' % np.max(test.labels))

    train_dist = np.bincount(np.array(train.labels).flatten())
    train_argmax = np.argmax(train_dist)
    print('Train most frequent class: %d (%f)'
          % (train_argmax, train_dist[train_argmax]/len(train.labels)),
          file=sys.stderr)

    valid_dist = np.bincount(np.array(valid.labels).flatten())
    valid_argmax = np.argmax(valid_dist)
    print('Valid most frequent class: %d (%f)'
          % (valid_argmax, valid_dist[valid_argmax]/len(valid.labels)),
          file=sys.stderr)

    test_dist = np.bincount(np.array(test.labels).flatten())
    test_argmax = np.argmax(test_dist)
    print('Test most frequent class: %d (%f)'
          % (test_argmax, test_dist[test_argmax]/len(test.labels)),
          file=sys.stderr)

    vocab_size = len(train.words_vocab)
    print('Vocab size %d' % vocab_size, file=sys.stderr)
    print('/Stats ===============', file=sys.stderr)


def main(dataset_first_n):
    # Data
    train_set = Dstc2('data/dstc2/data.dstc2.train.json', sample_unk=0.01,
                      first_n=dataset_first_n)
    valid_set = Dstc2('data/dstc2/data.dstc2.dev.json',
                      first_n=dataset_first_n, sample_unk=0,
                      max_dial_len=train_set.max_dial_len,
                      words_vocab=train_set.words_vocab,
                      labels_vocab=train_set.labels_vocab,
                      labels_vocab_separate=train_set.labels_vocab_separate)
    test_set = Dstc2('data/dstc2/data.dstc2.test.json',
                     first_n=dataset_first_n, sample_unk=0,
                     max_dial_len=train_set.max_dial_len,
                     words_vocab=train_set.words_vocab,
                     labels_vocab=train_set.labels_vocab,
                     labels_vocab_separate=train_set.labels_vocab_separate)
    print_stats(train_set, valid_set, test_set)

    # Define model.
    model = SimpleMatchModel(
        words_vocab=train_set.words_vocab,
        labels_vocab=train_set.labels_vocab,
        labels_vocab_separate=train_set.labels_vocab_separate
    )

    # The SimpleMatchModel does not need any training, so we can go
    # straight to evaluation.
    predicted_labels = model.predict(valid_set.model_input_iterator,
                                     valid_set.labels_separate.shape)
    eval_results = evaluate(
        valid_set.labels_separate, predicted_labels, valid_set.dial_mask,
        true_negative_idx=valid_set.labels_vocab_separate[0].get_i('none')
    )
    print(format_results(eval_results, main_result_key='joint_turn_accuracy'))
    return model, eval_results


if __name__ == "__main__":
    ARGS = get_args()
    main(**vars(ARGS))
