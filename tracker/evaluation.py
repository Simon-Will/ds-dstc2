#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from itertools import product
from functools import partial


def joint_turn_accuracy(gold, predicted, mask, *args, **kwargs):
    correct = 0
    total = 0
    for dialog_idx, turn_idx in product(range(gold.shape[0]),
                                        range(gold.shape[1])):
        if mask[dialog_idx, turn_idx]:
            total += 1
            if all(g == p for g, p in zip(gold[dialog_idx, turn_idx],
                                          predicted[dialog_idx, turn_idx])):
                correct += 1
    return correct / total


def single_label_turn_accuracy(label_idx, gold, predicted, mask,
                               *args, **kwargs):
    correct = 0
    total = 0
    for dialog_idx, turn_idx in product(range(gold.shape[0]),
                                        range(gold.shape[1])):
        if mask[dialog_idx, turn_idx]:
            total += 1
            if (gold[dialog_idx][turn_idx][label_idx]
                    == predicted[dialog_idx][turn_idx][label_idx]):
                correct += 1
    return correct / total


def single_label_turn_precision(label_idx, gold, predicted, mask,
                                true_negative_idx):
    true_positive = 0
    all_positive = 0
    for dialog_idx, turn_idx in product(range(gold.shape[0]),
                                        range(gold.shape[1])):
        if mask[dialog_idx, turn_idx]:
            if predicted[dialog_idx][turn_idx][label_idx] != true_negative_idx:
                all_positive += 1
                if (gold[dialog_idx][turn_idx][label_idx]
                        == predicted[dialog_idx][turn_idx][label_idx]):
                    true_positive += 1
    try:
        precision = true_positive / all_positive
    except ZeroDivisionError:
        precision = 0
    return precision


def single_label_turn_recall(label_idx, gold, predicted, mask,
                             true_negative_idx):
    true_positive = 0
    true_positive_and_false_negative = 0
    for dialog_idx, turn_idx in product(range(gold.shape[0]),
                                        range(gold.shape[1])):
        if mask[dialog_idx, turn_idx]:

            if gold[dialog_idx][turn_idx][label_idx] != true_negative_idx:
                true_positive_and_false_negative += 1
                if (gold[dialog_idx][turn_idx][label_idx]
                        == predicted[dialog_idx][turn_idx][label_idx]):
                    true_positive += 1
    return true_positive / true_positive_and_false_negative


def f1(precision_key: str, recall_key: str, metrics: dict):
    precision = metrics[precision_key]
    recall = metrics[recall_key]
    try:
        f1 = 2 * precision * recall / (precision + recall)
    except ZeroDivisionError:
        f1 = 0
    return f1


META_METRICS = ['food_turn_f1', 'area_turn_f1', 'pricerange_turn_f1']

ALL_METRICS = [
    'joint_turn_accuracy',
    'food_turn_accuracy', 'area_turn_accuracy', 'pricerange_turn_accuracy',
    'food_turn_recall', 'area_turn_recall', 'pricerange_turn_recall',
    'food_turn_precision', 'area_turn_precision', 'pricerange_turn_precision',
    'food_turn_f1', 'area_turn_f1', 'pricerange_turn_f1'
]

METRIC_TO_EVALUATOR = {
    'joint_turn_accuracy': joint_turn_accuracy,
    'food_turn_accuracy': partial(single_label_turn_accuracy, 0),
    'area_turn_accuracy': partial(single_label_turn_accuracy, 1),
    'pricerange_turn_accuracy': partial(single_label_turn_accuracy, 2),
    'food_turn_recall': partial(single_label_turn_recall, 0),
    'area_turn_recall': partial(single_label_turn_recall, 1),
    'pricerange_turn_recall': partial(single_label_turn_recall, 2),
    'food_turn_precision': partial(single_label_turn_precision, 0),
    'area_turn_precision': partial(single_label_turn_precision, 1),
    'pricerange_turn_precision': partial(single_label_turn_precision, 2),
    'food_turn_f1': partial(f1, 'food_turn_precision', 'food_turn_recall'),
    'area_turn_f1': partial(f1, 'area_turn_precision', 'area_turn_recall'),
    'pricerange_turn_f1': partial(f1, 'pricerange_turn_precision',
                                  'pricerange_turn_recall'),
}


def evaluate(gold, predicted, mask, true_negative_idx=-1, metrics=ALL_METRICS):
    assert(gold.shape[:2] == predicted.shape[:2] == mask.shape)

    results = {}
    meta_metrics = []
    for metric in metrics:
        if metric in META_METRICS:
            meta_metrics.append(metric)
        else:
            results[metric] = METRIC_TO_EVALUATOR[metric](
                gold, predicted, mask, true_negative_idx
            )

    for metric in meta_metrics:
        results[metric] = METRIC_TO_EVALUATOR[metric](results)

    return results


def asterisk_surround(s: str):
    return '*** {} ***'.format(s)


def format_results(results, main_result_key=None, highlight=asterisk_surround):
    lines = []
    for key, result in sorted(results.items()):
        formatted = '{}: {:.2f}'.format(key, result)
        if key == main_result_key:
            lines.insert(0, highlight(formatted))
        else:
            lines.append(formatted)
    return '\n'.join(lines)
