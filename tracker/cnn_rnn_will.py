#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import random
import sys

import numpy as np
import torch
from torch import nn
import torch.nn.functional as F

from tracker.dataset.dstc2 import Dstc2
from tracker.evaluation import evaluate, format_results


class CNNPlusRNN(nn.Module):

    def __init__(self, input_vocab_size, food_vocab_size, area_vocab_size,
                 pricerange_vocab_size, embedding_size=30, kernel_sizes=(2, 3),
                 out_channels=2, rnn_hidden_size=20, dropout_prob=0.5):
        super().__init__()

        self.embed = nn.Embedding(input_vocab_size, embedding_size)

        # Convolution similar to Sentence Classification CNN
        # for each turn.
        in_channels = 1
        self.convs = nn.ModuleList([
            nn.Conv2d(in_channels, out_channels, (kernel_size, embedding_size))
            for kernel_size in kernel_sizes
        ])
        self.convs_pooled_size = out_channels * len(kernel_sizes)

        # RNN for processing a sequence of turns.
        self.rnn = nn.GRU(self.convs_pooled_size, rnn_hidden_size)

        # Final projection to the output.
        self.food_projection = nn.Linear(rnn_hidden_size, food_vocab_size)
        self.area_projection = nn.Linear(rnn_hidden_size, area_vocab_size)
        self.pricerange_projection = nn.Linear(rnn_hidden_size,
                                               pricerange_vocab_size)

        self.dropout = nn.Dropout(dropout_prob)

    def forward(self, utterances):
        """Make a forward pass through the network.

        Does not support batches.

        :param utterances: turn_num x max_turn_len
        """
        turn_num, max_turn_len = utterances.shape
        # (turn_num, max_turn_len, embedding_size)
        embedded = self.embed(utterances)

        # (turn_num, convs_pooled_size)
        turn_vectors = torch.zeros((turn_num, self.convs_pooled_size))

        # (turn_num, batch_size = 1, in_channels = 1, max_turn_len,
        # embedding_size)
        embedded = embedded.unsqueeze(1).unsqueeze(1)
        for turn_idx in range(turn_num):

            # (batch_size = 1, out_channels, max_turn_len) * len(self.convs)
            convs_out = [F.relu(conv(embedded[turn_idx])).squeeze(3)
                         for conv in self.convs]

            # (out_channels,) * len(self.convs)
            convs_pooled = [
                F.max_pool1d(conv_out, conv_out.size(2)).squeeze(2).squeeze(0)
                for conv_out in convs_out
            ]

            # (convs_pooled_size,)
            turn_vectors[turn_idx] = torch.cat(convs_pooled, 0)

        # (turn_num, batch_size = 1, convs_pooled_size)
        turn_vectors = turn_vectors.unsqueeze(1)

        # (turn_num, rnn_hidden_size)
        rnn_out = self.rnn(turn_vectors)[0].squeeze(1)

        food_logits = self.food_projection(rnn_out[-1])
        area_logits = self.area_projection(rnn_out[-1])
        pricerange_logits = self.pricerange_projection(rnn_out[-1])

        return food_logits, area_logits, pricerange_logits


class CombinedLoss(nn.Module):
    def forward(self, losses):
        return sum(loss for loss in losses) / len(losses)


def train(model, train_set, epochs=5, log_freq=50):
    train_idx_set = []
    for dialog_idx in range(len(train_set._dials)):
        for turn_idx in range(train_set._dial_lens[dialog_idx]):
            train_idx_set.append((dialog_idx, turn_idx))

    optimizer = torch.optim.Adam(model.parameters())

    step = 0
    max_steps = epochs * len(train_idx_set)
    try:
        for epoch in range(epochs):
            print('===== Starting epoch {}/{} ====='.format(epoch, epochs))
            random.shuffle(train_idx_set)
            for dialog_idx, turn_idx in train_idx_set:
                utterances = torch.LongTensor(
                    train_set.dialogs[dialog_idx][:turn_idx + 1])
                food_label, area_label, pricerange_label = (
                    train_set.labels_separate[dialog_idx][turn_idx])

                optimizer.zero_grad()
                food_logits, area_logits, pricerange_logits = model(utterances)

                loss = CombinedLoss()([
                    F.cross_entropy(food_logits.unsqueeze(0),
                                    torch.LongTensor([food_label])),
                    F.cross_entropy(area_logits.unsqueeze(0),
                                    torch.LongTensor([area_label])),
                    F.cross_entropy(pricerange_logits.unsqueeze(0),
                                    torch.LongTensor([pricerange_label])),
                ])
                loss.backward()
                optimizer.step()

                if step % log_freq == 0:
                    print('----- Step {}/{} ({:.2f} %) -----'
                          .format(step, max_steps, step / max_steps * 100))
                    print('Labels: {}, {}, {}'.format(
                        food_label, area_label, pricerange_label))
                    print('Loss: {}'.format(loss))

                step += 1

    except KeyboardInterrupt:
        print('Interrupted the training.')

    return model


def predict(model, inputs, labels_shape):
    predicted_labels = np.zeros(labels_shape, dtype=int)
    for dialog_idx, turn_idx, (turns, turn_lens, prev_labels) in inputs:
        food_logits, area_logits, pricerange_logits = model.forward(
            torch.LongTensor(turns))
        food_label = torch.argmax(food_logits).item()
        area_label = torch.argmax(area_logits).item()
        pricerange_label = torch.argmax(pricerange_logits).item()

        predicted_labels[dialog_idx][turn_idx][0] = food_label
        predicted_labels[dialog_idx][turn_idx][0] = area_label
        predicted_labels[dialog_idx][turn_idx][0] = pricerange_label
    return predicted_labels


def get_args():
    ap = argparse.ArgumentParser(__doc__)
    ap.add_argument('--dataset_first_n', default=None,
                    help='Reduce the datasets to first_n examples')
    args = ap.parse_args()
    return args


def print_stats(train, valid, test):
    print('Stats ===============')
    print('Train classes: %d' % np.max(train.labels))
    print('Valid classes: %d' % np.max(valid.labels))
    print('Test classes: %d' % np.max(test.labels))

    train_dist = np.bincount(np.array(train.labels).flatten())
    train_argmax = np.argmax(train_dist)
    print('Train most frequent class: %d (%f)'
          % (train_argmax, train_dist[train_argmax]/len(train.labels)),
          file=sys.stderr)

    valid_dist = np.bincount(np.array(valid.labels).flatten())
    valid_argmax = np.argmax(valid_dist)
    print('Valid most frequent class: %d (%f)'
          % (valid_argmax, valid_dist[valid_argmax]/len(valid.labels)),
          file=sys.stderr)

    test_dist = np.bincount(np.array(test.labels).flatten())
    test_argmax = np.argmax(test_dist)
    print('Test most frequent class: %d (%f)'
          % (test_argmax, test_dist[test_argmax]/len(test.labels)),
          file=sys.stderr)

    vocab_size = len(train.words_vocab)
    print('Vocab size %d' % vocab_size, file=sys.stderr)
    print('/Stats ===============', file=sys.stderr)


def main(dataset_first_n):
    # Data
    train_set = Dstc2('data/dstc2/data.dstc2.train.json', sample_unk=0.01,
                      first_n=dataset_first_n)
    valid_set = Dstc2('data/dstc2/data.dstc2.dev.json',
                      first_n=dataset_first_n, sample_unk=0,
                      max_dial_len=train_set.max_dial_len,
                      words_vocab=train_set.words_vocab,
                      labels_vocab=train_set.labels_vocab,
                      labels_vocab_separate=train_set.labels_vocab_separate)
    test_set = Dstc2('data/dstc2/data.dstc2.test.json',
                     first_n=dataset_first_n, sample_unk=0,
                     max_dial_len=train_set.max_dial_len,
                     words_vocab=train_set.words_vocab,
                     labels_vocab=train_set.labels_vocab,
                     labels_vocab_separate=train_set.labels_vocab_separate)
    print_stats(train_set, valid_set, test_set)

    # Define model.
    model = CNNPlusRNN(
        input_vocab_size=len(train_set.words_vocab._int2w),
        food_vocab_size=len(train_set.labels_vocab_separate[0]),
        area_vocab_size=len(train_set.labels_vocab_separate[1]),
        pricerange_vocab_size=len(train_set.labels_vocab_separate[2]),
    )
    model = train(model, train_set)

    predicted_labels = predict(model, valid_set.model_input_iterator,
                               valid_set.labels_separate.shape)
    eval_results = evaluate(
        valid_set.labels_separate, predicted_labels, valid_set.dial_mask,
        true_negative_idx=valid_set.labels_vocab_separate[0].get_i('none')
    )
    print(format_results(eval_results, main_result_key='joint_turn_accuracy'))
    return model, eval_results


if __name__ == "__main__":
    ARGS = get_args()
    main(**vars(ARGS))
