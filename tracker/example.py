#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Stub to fill in ML model
"""
import argparse
import sys
import numpy as np
import tensorflow as tf

from tracker.dataset.dstc2 import Dstc2


def get_args():
    ap = argparse.ArgumentParser(__doc__)
    ap.add_argument('--dataset_first_n', default=None, help='Reduce the datasets to first_n examples')
    FLAGS, unparsed = ap.parse_known_args()
    return FLAGS, unparsed


def print_stats(train, valid, test):
    print('Stats ===============')
    print('Train classes: %d' % np.max(train.labels))
    print('Valid classes: %d' % np.max(valid.labels))
    print('Test classes: %d' % np.max(test.labels))

    train_dist = np.bincount(np.array(train.labels).flatten())
    train_argmax = np.argmax(train_dist)
    print('Train most frequent class: %d (%f)' % (train_argmax, train_dist[train_argmax]/len(train.labels)), file=sys.stderr)

    valid_dist = np.bincount(np.array(valid.labels).flatten())
    valid_argmax = np.argmax(valid_dist)
    print('Valid most frequent class: %d (%f)' % (valid_argmax, valid_dist[valid_argmax]/len(valid.labels)), file=sys.stderr)

    test_dist = np.bincount(np.array(test.labels).flatten())
    test_argmax = np.argmax(test_dist)
    print('Test most frequent class: %d (%f)' % (test_argmax, test_dist[test_argmax]/len(test.labels)), file=sys.stderr)

    vocab_size = len(train.words_vocab)
    print('Vocab size %d' % vocab_size, file=sys.stderr)
    print('/Stats ===============', file=sys.stderr)


def main(_):

    # Data --------------------------------------------------------------------
    train_set = Dstc2('data/dstc2/data.dstc2.train.json', sample_unk=0.01,
                      first_n=FLAGS.dataset_first_n)
    valid_set = Dstc2('data/dstc2/data.dstc2.dev.json',
                      first_n=FLAGS.dataset_first_n, sample_unk=0,
                      max_dial_len=train_set.max_dial_len,
                      words_vocab=train_set.words_vocab,
                      labels_vocab=train_set.labels_vocab,
                      labels_vocab_separate=train_set.labels_vocab_separate)
    test_set = Dstc2('data/dstc2/data.dstc2.test.json',
                     first_n=FLAGS.dataset_first_n, sample_unk=0,
                     max_dial_len=train_set.max_dial_len,
                     words_vocab=train_set.words_vocab,
                     labels_vocab=train_set.labels_vocab,
                     labels_vocab_separate=train_set.labels_vocab_separate)

    print_stats(train_set, valid_set, test_set)


if __name__ == "__main__":
    FLAGS, unparsed = get_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
